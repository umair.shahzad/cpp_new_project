#include <iostream>
#include "student.h"
#include <iterator>

student::student(const std::string &new_roll_no, const int &new_age, const float &new_cgpa)
{
    std::cout << "Parametrized constructor called\n";
    data.roll_no = new_roll_no;
    data.age = new_age;
    data.cgpa = new_cgpa;
}

student::student()
{
    std::cout << "Default constructor called\n";
    data.roll_no = "Nil";
    data.age = -1;
    data.cgpa = -1;
    std::cout << "Roll Number : " << data.roll_no << std::endl;
    std::cout << "Age : " << data.age << std::endl;
    std::cout << "CGPA : " << data.cgpa << std::endl;
}

bool student::set_subject_marks(const std::string &subject, const int &marks)
{
    if (marks >= 0 && marks <= 100)
    {
        result[subject] = marks;
        std::cout << "Marks of " << subject << " updated. \n";
        std::cout << "\n----------------------------------------------\n";

        return true;
    }
    else
    {
        std::cout << "Wrong Input.\n";
        std::cout << "\n----------------------------------------------\n";

        return false;
    }
}

int student::get_subject_marks(const std::string &subject)
{
    if (result.find(subject) != result.end())
        return result.find(subject)->second;
    else
        return -1;
}

void student::print_all_marks()
{
    std::map<std::string, int>::iterator it;
    std::cout << "subject marks of Roll_no " << data.roll_no << " is given below" << std::endl;
    std::cout << "\n----------------------------------------------\n";
    std::cout << "Subject\t\tmarks" << std::endl;
    if (result.empty())
    {
        std::cout << "No data found" << std::endl;
        return;
    }
    for (it = result.begin(); it != result.end(); ++it)
    {
        std::cout << it->first;
        std::cout << '\t' << it->second << '\n'
                  << std::endl;
    }
}

std::string student::get_roll_no() const
{
    return data.roll_no;
}

void student::set_roll_no(const std::string &new_roll_no)
{
    data.roll_no = new_roll_no;
    std::cout << "Roll number updated\n";
    std::cout << "\n----------------------------------------------\n";
}

int student::get_age() const
{
    return data.age;
}

bool student::set_age(const int &new_age)
{
    if (new_age >= 4 && new_age <= 45)
    {
        data.age = new_age;
        std::cout << "Age updated.\n";
        std::cout << "\n----------------------------------------------\n";

        return true;
    }
    else
    {
        std::cout << "Wrong input.\n";
        std::cout << "\n----------------------------------------------\n";

        return false;
    }
}

float student::get_cgpa() const
{
    return data.cgpa;
}

bool student::set_cgpa(const float &new_cgpa)
{
    if (new_cgpa >= 0 && new_cgpa <= 100)
    {
        data.cgpa = new_cgpa;
        std::cout << "Cgpa updated\n";
        std::cout << "\n----------------------------------------------\n";

        return true;
    }
    else
    {
        std::cout << "Wrong input\n";
        std::cout << "\n----------------------------------------------\n";

        return false;
    }
}
