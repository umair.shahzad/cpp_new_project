#ifndef INCLUDE_STUDENT_H
#define INCLUDE_STUDENT_H

#include <string>
#include <map>

class student
{
private:
  struct student_record
  {
    std::string roll_no;
    int age;
    float cgpa;
  } data;

  std::map<std::string /*subject name*/, int /*marks*/> result;

public:
  student();
  student(const std::string &new_roll_no, const int &new_age, const float &new_cgpa);
  int get_subject_marks(const std::string &subject);
  bool set_subject_marks(const std::string &subject, const int &marks);
  void print_all_marks();
  std::string get_roll_no() const;
  void set_roll_no(const std::string &new_roll_no);
  int get_age() const;
  bool set_age(const int &new_age);
  float get_cgpa() const;
  bool set_cgpa(const float &new_cgpa);

  /*
  Implement your get and set functions here to read/write the roll_no,
  age and cgpa.
  You can declare any private variables too if necessary.
  */
};

#endif