# cpp_new_project

## Initial Project Setup
- Made directories **inlcude** **src** **app**.

- Configured the **student.h** and **whitelist.h** with the function definitions and put these files in **include** directory.

- Implemented all the functions in **student.cpp** and **whitelist.cpp** move these files in **src** directory 

- Wrote a main.cpp file to check whether the functions implemented in **include/student.cpp** and **include/whitelist.cpp** are working or not.

- Moved **main.cpp** file in **app** directory

- Made 3 **CMakeLists.txt(main directory)** **src/CMakeLists.txt** and **include/CMakeLists.txt**.

- For debug and release modes use **cmake -DCMAKE_BUILD_TYPE = type ..**. Type can be **Debug** **Release** and 2 more options are there.

## Separate the Declaration of Class from Implementation
- Configured **inlcude/student.h** only for declaration of function and student class.

- Configured **inlcude/whitelist.h** only for declaration of function and student class.

- Implemented all the functions in **src/student.cpp** of **inlcude/student.h**. 

- Implemented all the functions in **src/whitelist.cpp** of **inlcude/whitelist.h**.

- Made static library of class **student.cpp** naming **data_student** and linked it to executable **./Cpp_project_(Debug or release)**.


- Made static library of class **whitelist.cpp** naming **data_student** and linked it to executable **./Cpp_project_(Debug or release)**.


## Installation
- Clone the repostiory in your local system.

- Get in the repository. Make a **build** directory and go in build. 

- Inside build directory call the command **cmake ..**

- Call **make** in the same respository and the executable file will build  in the build/app directory.

- To run the executable call **./Cpp_project_release** or go in **build/** and call **./Cpp_project_release**
