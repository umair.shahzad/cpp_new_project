#include <iostream>
#include "whitelist.h"

void load_data(whitelist &student_record);
int main()
{
    student *ptr;
    whitelist student_record;
    std::string student_name;
    std::cout << "\n----------------------------------------------\n";

    std::cout << "Whitelist student data is empty\n";

    std::cout << "\n----------------------------------------------\n";

    student_record.print_whitelist_student_data();

    std::cout << "\n----------------------------------------------\n";

    load_data(student_record);

    std::cout << "\n----------------------------------------------\n";

    std::cout << "Added 15 students data\n";

    std::cout << "\n----------------------------------------------\n";

    student_record.print_whitelist_student_data();

    std::cout << "\n----------------------------------------------\n";

    std::cout << "Searching for student " << student_name << " in data\n";
    student_name = "umair";

    std::cout << "\n----------------------------------------------\n";

    if (student_record.is_student_present(student_name))
    {

        std::cout << "student named " << student_name << " data is given below" << '\n';
        ptr = student_record.get_student_data(student_name);

        std::cout << "Roll_no\t " << ptr->get_roll_no() << std::endl;
        std::cout << "Age\t " << ptr->get_age() << std::endl;
        std::cout << "cgpa\t " << ptr->get_cgpa() << std::endl;

        std::cout << "\n----------------------------------------------\n";

        std::cout << "Setting parameters through functions\n";

        std::cout << "\n----------------------------------------------\n";

        ptr->set_roll_no("17i-0447");
        ptr->set_age(21);
        ptr->set_cgpa(3.3);

        std::cout << "\n----------------------------------------------\n";

        std::cout << "No subject marks added yet!\n";

        std::cout << "\n----------------------------------------------\n";

        std::cout << "Printing All subjects marks\n";

        ptr->print_all_marks();

        std::cout << "\n----------------------------------------------\n";

        ptr->set_subject_marks("Programming", 90);
        ptr->set_subject_marks("Mathematics", 70);
        ptr->print_all_marks();

        std::cout << "\n----------------------------------------------\n";
    }
    else
    {
        std::cout << "Student data not found!\n";
    }
    return 0;
}

void load_data(whitelist &student_record)
{
    std::string student_name[15] = {"usama", "umair", "adnan", "huzaifa", "farakh", "usman", "moazzum", "munib", "jawad", "hamza", "subhan", "zill", "john", "ahmed", "ali"};
    for (int index = 0; index < 15; ++index)
    {
        std::cout << "\n----------------------------------------------\n";
        student student(std::to_string(index + 1), 23, 4);
        student_record.add_to_whitelist(student_name[index], student);
    }
}
