#ifndef INCLUDE_WHITELIST_H
#define INCLUDE_WHITELIST_H

#include "student.h"
#include <map>
#include <list>
class whitelist
{
private:
    std::map<std::string, student *> student_record;
    std::list<student> student_data_list;

public:
    void add_to_whitelist(std::string &, student &);
    bool is_student_present(const std::string &) const;
    student *get_student_data(const std::string &) const;
    void print_whitelist_student_data();
    ~whitelist();
};

#endif