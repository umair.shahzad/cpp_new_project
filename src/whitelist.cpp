#include <iostream>
#include "whitelist.h"

bool whitelist::is_student_present(const std::string &name) const
{
    if (student_record.find(name) != student_record.end())
        return true;
    else
        return false;
}

void whitelist::add_to_whitelist(std::string &name, student &info)
{
    if (is_student_present(name))
    {
        std::cout << "Student data already present" << '\n';
    }
    else
    {
        student_data_list.push_back(info);
        student_record[name] = &student_data_list.back();
        std::cout << "Student data added" << '\n';
    }
}

student *whitelist::get_student_data(const std::string &name) const
{
    return student_record.find(name)->second;
}

void whitelist::print_whitelist_student_data()
{
    std::map<std::string, student *>::iterator it;

    if (student_record.empty())
    {
        std::cout << "No data found" << std::endl;
        return;
    }
    for (it = student_record.begin(); it != student_record.end(); ++it)
    {
        std::cout << "Name\t " << it->first << '\n';
        std::cout << "Roll_no\t " << it->second->get_roll_no() << std::endl;
        std::cout << "Age\t " << it->second->get_age() << std::endl;
        std::cout << "cgpa\t " << it->second->get_cgpa() << std::endl;
        it->second->print_all_marks();
        std::cout << "\n----------------------------------------------\n";
    }
}

whitelist::~whitelist()
{

    std::map<std::string, student *>::iterator it;

    if (!student_record.empty())
    {
        return;
    }
    for (it = student_record.begin(); it != student_record.end(); ++it)
    {
        delete it->second;
        student_record.erase(it);
    }
}
